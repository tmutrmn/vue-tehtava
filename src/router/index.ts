import { createRouter, createWebHashHistory, RouteRecordRaw } from 'vue-router'
import Home from '../views/Home.vue'

const routes: Array<RouteRecordRaw> = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/peoples',
    name: 'Peoples',
    component: () => import(/* webpackChunkName: "people" */ '../views/Peoples.vue')
  },
  {
    path: '/peoples/add',
    name: 'AddPeople',
    component: () => import(/* webpackChunkName: "people" */ '../views/AddPeople.vue')
  },
  {
      path: '/people/:id',
      name: 'People',
      props: true,
      component: () => import ('../views/People.vue')
  },
  {
    path: '/planets',
    name: 'Planets',
    component: () => import(/* webpackChunkName: "planets" */ '../views/Planets.vue')
  },
  {
      path: '/planet/:id',
      name: 'Planet',
      props: true,
      component: () => import ('../views/Planet.vue')
  },
  {
    path: '/starships',
    name: 'Starships',
    component: () => import(/* webpackChunkName: "starships" */ '../views/Starships.vue')
  },
  {
      path: '/starship/:id',
      name: 'Starship',
      props: true,
      component: () => import ('../views/Starship.vue')
  },
  {
    path: '/complete',
    name: 'Complete',
    component: () => import(/* webpackChunkName: "about" */ '../views/Complete.vue')
  },
  {
    path: '/about',
    name: 'About',
    component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
  }
]

const router = createRouter({
  history: createWebHashHistory(),
  routes
})

export default router
