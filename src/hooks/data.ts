import useApi from "./api";
import { ref } from "vue";

export default async function useData(url: string) {
  const { response: result, request } = useApi(url);

  const loaded = ref(false);
  if (loaded.value === false) {
    await request();
    loaded.value = true;
  }

  return { result };
}