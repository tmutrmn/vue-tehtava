import { createStore, createLogger } from 'vuex';
import { ICharacter, IPlanet, IStarship } from '@/interfaces';


export interface State {
  loading: boolean;
  characters: ICharacter[];
  selectedCharacter: ICharacter;
  selectedPlanet: IPlanet;
  selectedStarship: IStarship;
}

export const store = createStore<State>({
  plugins: [createLogger()],

  state: {
    loading: true,
    characters: [],
    selectedCharacter: {} as ICharacter,
    selectedPlanet: {} as IPlanet,
    selectedStarship: {} as IStarship,
  },

  getters: {

  },

  mutations: {
    setLoading(state: any, value: any) {
      state.loading = value
    },
    setCharacters(state: any, value: any) {
      state.characters = value
    },
    setSelectedCharacter(state: any, character: ICharacter) {
      state.selectedCharacter = character
    },
    setSelectedPlanet(state: any, planet: IPlanet) {
      state.selectedPlanet = planet
    },
    setSelectedStarship(state: any, starship: IStarship) {
      state.selectedStarship = starship
    },
  },

  actions: {
    // loadData ()
  },

  modules: {
  }
})
