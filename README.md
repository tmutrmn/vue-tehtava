# vue-tehtava

This simple Vue 3 app lets you select your favorite Star Wars character, planet and starship.
App also lets you delete or make your own characters.

App demonstrates using following Vue 3 features:

- Reusable components
- Templating
- Routing
- Handling state
- Reactivity
- Hooks to load REST API data

TODO:
- refactor some components
- fix actions and mutations in store
- write unit tests


## Project setup
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn serve
```

### Compiles and minifies for production
```
yarn build
```

### Lints and fixes files
```
yarn lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
